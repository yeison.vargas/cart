//
//  Created by Yeison Vargas
//  Copyright © 2018 Gnosoft Ltda. All rights reserved.
//
//  ### LEGAL NOTICE ###
//  This file is subject to the terms and conditions defined in
//  file 'LICENSE.txt', which is part of this source code package.
//

import Foundation

class ProductsWorker {
    var productsStore: ProductsStoreProtocol

    init(productsStore: ProductsStoreProtocol) {
        self.productsStore = productsStore
    }

    func fetchProducts(completionHandler: @escaping ([Product]) -> Void) {
        productsStore.fetchProducts { (products: () throws -> [Product]) -> Void in
            do {
                let products = try products()
                DispatchQueue.main.async {
                    completionHandler(products)
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandler([])
                }
            }
        }
    }

}

protocol ProductsStoreProtocol {
    // MARK: CRUD operations - Optional error

    func fetchProducts(completionHandler: @escaping ([Product], ProductsStoreError?) -> Void)

    // MARK: CRUD operations - Generic enum result type

    func fetchProducts(completionHandler: @escaping ProductsStoreFetchProductsCompletionHandler)

    // MARK: CRUD operations - Inner closure

    func fetchProducts(completionHandler: @escaping (() throws -> [Product]) -> Void)
}

protocol ProductsStoreUtilityProtocol {}

extension ProductsStoreUtilityProtocol {
}

// MARK: - Products store CRUD operation results

typealias ProductsStoreFetchProductsCompletionHandler = (ProductsStoreResult<[Product]>) -> Void

enum ProductsStoreResult<U> {
    case Success(result: U)
    case Failure(error: ProductsStoreError)
}

// MARK: - Products store CRUD operation errors

enum ProductsStoreError: Equatable, Error {
    case CannotFetch(String)
    case CannotCreate(String)
    case CannotUpdate(String)
    case CannotDelete(String)
}

extension ProductsStoreError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .CannotFetch(let message):
            return NSLocalizedString(message, comment: "Shuup can't fetch")
        case .CannotCreate(let message):
            return NSLocalizedString(message, comment: "Shuup can't create")
        case .CannotUpdate(let message):
            return NSLocalizedString(message, comment: "Shuup can't update")
        case .CannotDelete(let message):
            return NSLocalizedString(message, comment: "huup can't delete")
        }
    }
}

func == (lhs: ProductsStoreError, rhs: ProductsStoreError) -> Bool {
    switch (lhs, rhs) {
    case (.CannotFetch(let itemA), .CannotFetch(let itemB)) where itemA == itemB: return true
    case (.CannotCreate(let itemA), .CannotCreate(let itemB)) where itemA == itemB: return true
    case (.CannotUpdate(let itemA), .CannotUpdate(let itemB)) where itemA == itemB: return true
    case (.CannotDelete(let itemA), .CannotDelete(let itemB)) where itemA == itemB: return true
    default: return false
    }
}
