//
//  CartViewController.swift
//  shuup
//
//  Created by Yeison Vargas on 3/15/19.
//  Copyright (c) 2019 Rokk3rlabs. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit


class ProductInCartTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var delete: UIButton!
}


class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var interactor: ListProductsBusinessLogic?
    var productsViewController: ListProductsViewController?

    var cart: [Int: ShoopingCart.ViewModel.DisplayedProductInCart] = [:]
    var products: [ListProducts.FetchProducts.ViewModel.DisplayedProduct] = []
    var cacheProducts: [ListProducts.FetchProducts.ViewModel.DisplayedProduct] = []


    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)

    }

    // MARK: Setup

    private func setup()
    {
    }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {

  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
  }

    override func viewDidAppear(_ animated: Bool) {
        loadCartProducts()
    }
  
  // MARK: Do something
  
  //@IBOutlet weak var nameTextField: UITextField!
  
  func loadCartProducts()
  {
    tableView.reloadData()
  }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductInCartTableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductInCartTableViewCell") else {
                return UITableViewCell(style: .default, reuseIdentifier: "ProductInCartTableViewCell")
                    as! ProductInCartTableViewCell
            }
            return cell as! ProductInCartTableViewCell
        }()


        cell.name.text = Array(cart.values)[indexPath.row].name
        cell.quantity.text = String(Array(cart.values)[indexPath.row].quantity)
        cell.delete.tag = Array(cart.values)[indexPath.row].id

        cell.delete.addTarget(self, action: #selector(self.handleTap(_:)), for: .touchUpInside)


        return cell
    }

    @objc func handleTap(_ sender: UIButton) {
        let productId = sender.tag
        print("TAG")
        print(productId)

        var product : ListProducts.FetchProducts.ViewModel.DisplayedProduct

        if let pd = products.first(where: { $0.id == productId }) {
            product = pd
        } else {
            product = cacheProducts.first { $0.id == productId }!
            product.stock = "0"
        }

        interactor?.updateStock(request: ListProducts.UpdateStock.Request(remove: false, product: product))

        cart[productId] = ShoopingCart.ViewModel.DisplayedProductInCart(id: productId,
                                                                        name: cart[productId]!.name,
                                                                                 quantity: cart[productId]!.quantity - 1)
        var toRemove: [Int] = []

        for item in cart.values {
            if item.quantity < 1 {
                toRemove.append(item.id)
            }
        }

        toRemove.forEach { cart.removeValue(forKey: $0)}

        tableView.reloadData()

        productsViewController?.refreshCartData(refreshedCart: cart)



    }

    @IBAction func onTappedClosePopup(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
