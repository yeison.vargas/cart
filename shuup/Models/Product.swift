//
//  Product.swift
//  shuup
//
//  Created by Yeison Vargas on 3/15/19.
//  Copyright © 2019 Rokk3rlabs. All rights reserved.
//

import Foundation

struct Product: Equatable, Codable {
    var id: String
    var name: String
    var price: Double
    var stock: Int

    init(id: String, name: String, price: Double, stock: Int) {
        self.id = id
        self.name = name
        self.price = price
        self.stock = stock
    }

}
