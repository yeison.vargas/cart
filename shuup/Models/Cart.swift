//
//  Cart.swift
//  shuup
//
//  Created by Yeison Vargas on 3/15/19.
//  Copyright © 2019 Rokk3rlabs. All rights reserved.
//

import Foundation

struct Cart: Equatable, Codable {
    var products: [Product]

    init(products: [Product]) {
        self.products = products
    }
}
