//
//  ProductsMemStore.swift
//  shuup
//
//  Created by Yeison Vargas on 3/15/19.
//  Copyright © 2019 Rokk3rlabs. All rights reserved.
//

import Foundation

class ProductMemStore: ProductsStoreProtocol {

    var products = [
        Product(id: "1", name: "Samsung Galaxy Express 3", price: 49.88, stock: 3),
        Product(id: "2", name: "Samsung Galaxy S5", price: 29.88, stock: 5),
        Product(id: "3", name: "LG Phoenix 2", price: 109.88, stock: 15),
        Product(id: "4", name: "LG Stylo 2", price: 41.88, stock: 1),
        Product(id: "5", name: "Samsung Galaxy S7", price: 355.88, stock: 4),
        Product(id: "6", name: "Kyocero Hydro Shore", price: 49, stock: 9),
        Product(id: "7", name: "Motorola Moto G4 Plus", price: 88, stock: 60),
        Product(id: "8", name: "Samsung Galaxy S5", price: 108, stock: 33),
        Product(id: "9", name: "ZTE Fanfare 2", price: 208, stock: 3),
        Product(id: "10", name: "Alcatel Pixi GLITZ", price: 500, stock: 1),
        Product(id: "11", name: "LG K7", price: 1000, stock: 10),
        Product(id: "12", name: "Samsung Galaxy Express Prime", price: 999, stock: 200),
        Product(id: "13", name: "Samsung Galaxy On5", price: 754, stock: 1),
        Product(id: "14", name: "Samsung Galaxy J3", price: 428, stock: 2),
        Product(id: "15", name: "Samsung Galaxy S6", price: 418, stock: 3),
        Product(id: "16", name: "Samsung Galaxy S5", price: 578, stock: 4),
        Product(id: "17", name: "Samsung Galaxy Legend", price: 10.88, stock: 6),
        Product(id: "18", name: "Alcatel onetouch Pixi Avion", price: 1500, stock: 13),
        Product(id: "19", name: "Alcatel Onetouch Allura", price: 788, stock: 11),
        Product(id: "20", name: "Coolpad Catalyst", price: 900, stock: 20),
    ]


    func fetchProducts(completionHandler: @escaping ([Product], ProductsStoreError?) -> Void) {
        completionHandler(products, nil)
    }

    func fetchProducts(completionHandler: @escaping ProductsStoreFetchProductsCompletionHandler) {
    }

    func fetchProducts(completionHandler: @escaping (() throws -> [Product]) -> Void) {
        completionHandler { return products }
    }



}
